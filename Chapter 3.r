library(nycflights13)
library(tidyverse)
library(Lahman)

nycflights13::flights
str(nycflights13::flights)
summary(nycflights13::flights)
view(nycflights13::flights)

#Select all flights on January 1st:
jan1 <- filter(flights, month == 1, day == 1)

#Assigning and printing out the results:
(dec25 <- filter(flights, month == 12, day == 25))

#Flights that departed in November or December:
filter(flights, month == 11 | month == 12)
#A shorter line:
filter(flights, month %in% c(11, 12))

#Either of these lines selects flights with no delay by more than two hours
#in departure or arrival:
filter(flights, dep_delay <= 120, arr_delay <= 120)
filter(flights, !(dep_delay > 120 | arr_delay > 120))

#Flights that:
#had an arrival delay of 2 or more hours:
filter(flights, arr_delay >= 120)

#flew to Houston (IAH, HOU):
filter(flights, dest == "IAH" | dest == "HOU")
filter(flights, dest %in% c("IAH", "HOU"))

#were operated United, American or Delta:
filter(flights, carrier %in% c("UA", "AA", "DL"))

#departed in summer:
filter(flights, month %in% c(7, 8, 9))
#using between():
filter(flights, between(month, 7, 9))

#arrived more than 2 hours late, but didn't leave late:
filter(flights, arr_delay > 120 & dep_delay == 0)

#delayed by at least an hour, but made up over 30 minutes in flight:
filter(flights, dep_delay >= 60 & arr_delay < 30)

#departed between midnight and 6 am:
filter(flights, dep_time >= 0 & dep_time <= 600) #(dep_time >= 0 &) is optional
#using between():
filter(flights, between(dep_time, 0, 600)) 

#Using the arrange() function:
arrange(flights, year, month, day)
arrange(flights, desc(arr_delay))

#Missing values are always sorted at the end:
df <- tibble(x = c(5, 2, NA))
arrange(df, x)
arrange(df, desc(x))

#Using arrange() to sort all missing values to the start:
arrange(flights, desc(is.na(flights)))

#Finding the most delayed flights: 
arrange(flights, desc(dep_delay), desc(arr_delay))

#Finding the flights that left earliest:
arrange(flights, dep_delay)

#Finding the fastest flights:
arrange(flights, air_time)

#Selecting columns by name:
select(flights, year, month, day)

#Selecting all columns between year and day:
select(flights, year:day)

#Selecting all columns except those between year and day:
select(flights, -(year:day))

#Renaming variables/columns:
rename(flights, tail_num = tailnum)

#Moving variables to the start of the dataframe:
select(flights, time_hour, air_time, everything())

#Using one_of():
vars <- c("year", "month", "day", "dep_delay", "arr_delay")
select(flights, vars)

#Using contains():
select(flights, contains("TIME"))

#Using mutate():
flights_sml <- select(flights, year:day, ends_with("delay"), distance, air_time)
mutate(flights_sml, gain = arr_delay - dep_delay, speed = distance / air_time * 60)

#Referring to columns right after creating them:
mutate(flights_sml, gain = arr_delay - dep_delay, hours = air_time / 60, speed = distance / hours)

#Only keeping new variables:
transmute(flights, gain = arr_delay - dep_delay, hours = air_time / 60, gain_per_hour = gain / hours)

#Computing hour and minute from dep_time:
transmute(flights, hour = dep_time %/% 100, minute = dep_time %% 100)

#Converting dep_time and sched_dep_time into "minutes since midnight" format:
flights$dep_time <- ((flights$dep_time %/% 100) * 60) + (flights$dep_time %% 100)
flights$sched_dep_time <- ((flights$sched_dep_time %/% 100) * 60) + (flights$sched_dep_time %% 100)

select(flights, flights$dep_delay == flights$dep_time - flights$sched_dep_time)

#Finding the 10 most delayed flights:
arrange(flights, min_rank(desc(dep_delay)))

#Making a grouped summary of the average delay per date:
by_day <- group_by(flights, year, month, day)
summarize(by_day, delay = mean(dep_delay, na.rm = TRUE))

#Relationship between distance and average delay:
by_dest <- group_by(flights, dest)
delay <- summarize(by_dest, 
                   count = n(),
                   dist = mean(distance, na.rm = TRUE),
                   delay = mean(arr_delay, na.rm = TRUE))
delay <- filter(delay, count > 20, dest != "HNL")
delay
ggplot(data = delay, aes(x = dist, y = delay)) +
  geom_point(aes(size = count), alpha = 1/3) +
  geom_smooth(se = FALSE)
#Using the pipe:
delay <- flights %>%
  group_by(dest) %>%
  summarize(
    count = n(),
    dist = mean(distance, na.rm = TRUE),
    delay = mean(arr_delay, na.rm = TRUE)
  ) %>%
  filter(count > 20, dest != "HNL")

#Making a new dataset without canceled flights:
not_canceled <- flights %>%
  filter(!is.na(dep_delay), !is.na(arr_delay))
not_canceled %>%
  group_by(year, month, day) %>%
  summarize(mean = mean(dep_delay))

#Showing the frequency of average delays:
delays <- not_canceled %>%
  group_by(tailnum) %>%
  summarize(delay = mean(arr_delay))
ggplot(data = delays, aes(x = delay)) +
  geom_freqpoly(binwidth = 10)

#Number of flights vs average delay:
delays <- not_canceled %>%
  group_by(tailnum) %>%
  summarize(delay = mean(arr_delay, na.rm = TRUE), n = n())
ggplot(data = delays, aes(x = n, y = delay)) +
  geom_point(alpha = 1/10)

#The same as above, but with less extreme variation:
delays %>% 
  filter(n > 25) %>%
  ggplot(aes(x = n, y = delay)) +
  geom_point(alpha = 1/10)

#Skill of batter against number of opportunities:
batting <- as_tibble(Lahman::Batting)
batters <- batting %>%
  group_by(playerID) %>%
  summarize(
    ba = sum(H, na.rm = TRUE) / sum(AB, na.rm = TRUE),
    ab = sum(AB, na.rm = TRUE)
  )
batters %>% filter(ab > 100) %>%
  ggplot(aes(x = ab, y = ba)) +
  geom_point() +
  geom_smooth(se = FALSE)

#Finding the first and last flight of each day:
not_canceled %>%
  group_by(year, month, day) %>%
  summarize(first = first(dep_time), last = last(dep_time))

#Finding the destinations with the most carriers:
not_canceled %>%
  group_by(dest) %>%
  summarize(carriers = n_distinct(carrier)) %>%
  arrange(desc(carriers))

#Number of flights that left before 5  am:
not_canceled %>%
  group_by(year, month, day) %>%
  summarize(n_early = sum(dep_time < 300))

#Proportion of flights delayed by more than an hour:
not_canceled %>%
  group_by(year, month, day) %>%
  summarize(prop = mean(arr_delay > 60))

#Number of flights per day:
flights %>%
  group_by(year, month, day) %>%
  summarize(n())

#Number of flights per month:
flights %>%
  group_by(year, month) %>%
  summarize(n())

#Number of flights per year:
flights %>%
  group_by(year,) %>%
  summarize(n())

#Rewriting this code without using count():
not_canceled %>%
  count(dest)

not_canceled %>%
  group_by(dest) %>%
  summarize(n())

#Rewriting this code without using count():
not_canceled %>% 
  count(tailnum, wt = distance)

not_canceled %>%
  group_by(tailnum) %>%
  summarize(n = sum(distance))

#A better definition of canceled flights:
flights[is.na(flights$dep_time),]
filter(flights, is.na(dep_time))

#The number of canceled flights per day:
flights %>%
  group_by(year, month, day) %>%
  filter(is.na(dep_time)) %>%
  summarize(cancelled = n())

#Is the proportion of canceled flights related to the average delay?:
ti <- flights %>%
  group_by(year, month, day) %>%
  summarize(cp = mean(is.na(dep_time), na.rm = TRUE), 
            ad = mean(dep_delay, na.rm = TRUE)) %>%
  ggplot(aes(x = cp, y = ad)) +
  geom_point(alpha = 1/10) +
  geom_smooth(se = FALSE)

#Total minutes of delay per destination:
not_canceled %>%
  group_by(dest) %>%
  summarize(sum(arr_delay))

#All destinations that are flown to by at least 2 carriers:
not_canceled %>%
  group_by(dest) %>%
  summarize(carr = n_distinct(carrier)) %>%
  filter(carr >= 2) %>%
  arrange(desc(carr))
