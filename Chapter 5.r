library(tidyverse)

#A wide look at "cut" distribution:
ggplot(data = diamonds) +
  geom_bar(aes(x = cut))

diamonds %>%
  count(cut)

#A wide look at "carat" distribution:
ggplot(data = diamonds) +
  geom_histogram(aes(x = carat), binwidth = 0.5)

diamonds %>%
  count(cut_width(carat, 0.5))

#A look at "carat < 3" distribution:
smaller <- filter(diamonds, carat < 3)

ggplot(data = smaller) +
  geom_histogram(aes(x = carat), binwidth = 0.1)

#Multiple lines/bars for carats and cuts:
ggplot(data = smaller) +
  geom_freqpoly(aes(x = carat, color = cut), binwidth = 0.1)

ggplot(data = smaller) +
  geom_histogram(aes(x = carat, color = cut), binwidth = 0.01) 

#Finding outliers:
unusual <- filter(diamonds, y < 3 | y > 20) %>% arrange(y)
unusual

#Price distribution:
ggplot(data = diamonds, aes(x = price)) +
  geom_histogram(binwidth = 1000)


#How many diamonds are 0.99 carat and how many 1 carat:
diamonds %>%
  filter(carat == 0.99 | carat == 1) %>%
  arrange(carat) %>%
  count(carat)

diamonds %>% filter(carat == 0.99 | carat == 1) %>%
  ggplot(aes(x = carat)) +
  geom_bar()

#Replacing outliers with missing values:
diamonds2 <- diamonds %>%
  mutate(y = ifelse(y < 3 | y > 20, NA, y))

ggplot(data = diamonds2, aes(x = x, y = y)) +
  geom_point(na.rm = TRUE)

#The distribution of diamond price by cut:
ggplot(data = diamonds, aes(x = cut, y = price)) +
   geom_boxplot()

#How does highway mileage vary across classes?:
ggplot(data = mpg, aes(x = class, y = hwy)) +
  geom_boxplot()

#Same as above, with the classes reordered by hwy:
ggplot(data = mpg, aes(x = reorder(class, hwy, FUN = median) , y = hwy)) +
  geom_boxplot()
ggplot(data = mpg, aes(x = reorder(class, hwy, FUN = median) , y = hwy)) +
  geom_boxplot() + coord_flip()

#Relationship between two categorical variables, cut and color:
diamonds %>% 
  count(color, cut) %>%
  ggplot(aes(x = color, y = cut)) +
  geom_tile(aes(fill = n))

#Visualizing exponential relationship between diamond carat size and price:
ggplot(data = diamonds) +
  geom_point(aes(x = carat, y = price), alpha = 1/100)

#Binning in two dimensions:
ggplot(data = smaller) + 
  geom_bin_2d(aes(x = carat, y = price))
library(hexbin)
ggplot(data = smaller) +
  geom_hex(aes(x = carat, y = price))

#Visualizing the distribution of carat, partitioned by price:
ggplot(data = diamonds, aes(x = price, y = carat)) +
  geom_boxplot(aes(group = cut_width(price, 1000)), varwidth = TRUE)
