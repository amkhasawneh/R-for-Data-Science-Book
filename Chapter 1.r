#Importing the tidyverse packages:
library(tidyverse)

#Exploring the mpg dataset:
str(mpg)
ggplot(data = mpg) +
  geom_point(aes(x = displ, y = hwy))

#A scatterplot for hwy vs. cyl:
ggplot(data = mpg) +
  geom_point(aes(x = cyl, y = hwy))
#A scatterplot of class vs. drv:
ggplot(data = mpg) +
  geom_point(mapping = aes(x = class, y = drv))

#Mapping class to point colors:
ggplot(data = mpg) +
  geom_point(aes(x = displ, y = hwy, color = class))

#Mapping class to point sizes:
ggplot(data = mpg) +
  geom_point(aes(x = displ, y = hwy, size = class))

#Mapping class to point transparency:
ggplot(data = mpg) +
  geom_point(aes(x = displ, y = hwy, alpha = class))

#Mapping class to point shapes:
ggplot(data = mpg) +
  geom_point(aes(x = displ, y = hwy, shape = class))

#Setting point color to blue (outside aes()):
ggplot(data = mpg) +
  geom_point(aes(x = displ, y = hwy), color = "blue")

#Mapping  non-variable to point color:
ggplot(data = mpg) +
  geom_point(aes(x = displ, y = hwy, color = displ < 5))

#Creating a faceted graph:
ggplot(data = mpg) +
  geom_point(aes(x = displ, y = hwy)) +
  facet_wrap(. ~ class, nrow = 2)

#Creating a facet with two variables:
ggplot(data = mpg) +
  geom_point(aes(x = displ, y = hwy)) +
  facet_grid(drv ~ cyl)

#Faceting on a continuous variable:
ggplot(data = mpg) +
  geom_point(aes(x = displ, y = hwy)) +
  facet_grid(hwy ~ .)

#Adding a smooth geom:
ggplot(data = mpg) +
  geom_smooth(aes(x = displ, y = hwy, color = drv))

ggplot(data = mpg) +
  geom_smooth(aes(x = displ, y = hwy, linetype = drv))

ggplot(data = mpg) +
  geom_point(aes(x = displ, y = hwy, color = drv)) +
  geom_smooth(aes(x = displ, y = hwy, color = drv, linetype = drv))

ggplot(data = mpg) +
  geom_smooth(aes(x = displ, y = hwy, group = drv))

ggplot(data = mpg) +
  geom_smooth(aes(x = displ, y = hwy, color = drv), show.legend = FALSE)

#Passing the mapping to ggplot() (global mappings):
ggplot(data = mpg, aes(x = displ, y = hwy)) +
  geom_point(aes(color = class)) +
  geom_smooth()

#Data argument overriding:
ggplot(data = mpg, aes(x = displ, y = hwy)) +
  geom_point(aes(color = class)) +
  geom_smooth(data = filter(mpg, class == "subcompact"), se = FALSE)

ggplot(data = mpg, aes(x = displ, y = hwy)) +
  geom_point(aes(color = class)) +
  geom_smooth(data = mpg[mpg$class == "subcompact",], se = FALSE)

ggplot(data = mpg, aes(x = displ, y = hwy, color = drv)) +
  geom_point() +
  geom_smooth(se = FALSE)

#Exercises:
ggplot(data = mpg, aes(x = displ, y = hwy)) +
  geom_point() +
  geom_smooth(se = F)

ggplot(data = mpg, aes(x = displ, y = hwy)) +
  geom_point() +
  geom_smooth(aes(group = drv), se = FALSE)

ggplot(data = mpg, aes(x = displ, y = hwy, color = drv)) +
  geom_point() +
  geom_smooth(se = FALSE)

ggplot(data = mpg, aes(x = displ, y = hwy)) +
  geom_point(aes(color = drv)) +
  geom_smooth(se = FALSE)

ggplot(data = mpg, aes(x = displ, y = hwy)) +
  geom_point(aes(color = drv)) +
  geom_smooth(aes(linetype = drv), se = FALSE)

ggplot(data = mpg, aes(x= displ, y = hwy, color = drv)) +
  geom_point()

#The diamonds dataset, in a bar chart:
ggplot(data = diamonds) +
  geom_bar(aes(x = cut))

ggplot(data = diamonds) +
  stat_count(aes(x = cut))

#Overriding the default stat in the geom:
demo <- tribble(
  ~a, ~b,
  "bar_1", 20, 
  "bar_2", 30,
  "bar_3", 40
)
ggplot(data = demo) +
  geom_bar(aes(x = a, y = b), stat = "identity")

#Overriding the stat to show proportion:
ggplot(data = diamonds) +
  geom_bar(aes(x = cut, y = ..prop.., group = 1))

#Overriding the stat to draw attention to the summary:
ggplot(data = diamonds) +
  stat_summary(aes(x = cut, y = depth),
               fun.ymin = min,
               fun.ymax = max,
               fun.y = median)

#To show means:
ggplot(data = diamonds) +
  geom_col(aes(x = cut, y = depth))

ggplot(data = diamonds) +
  geom_bar(aes(x = cut, y = ..prop.., fill = color, group = 1))

#Coloring bars:
ggplot(data = diamonds) +
  geom_bar(aes(x = cut, color = cut))

ggplot(data = diamonds) +
  geom_bar(aes(x = cut, fill = cut))

#Mapping the fill aesthetic to another variable stacks the bars:
ggplot(data = diamonds) +
  geom_bar(aes(x = cut, fill = clarity))

#Other options for the "position" argument:
#1
ggplot(data = diamonds, aes(x = cut, fill = clarity)) +
  geom_bar(position = "identity", alpha = 1/5)

ggplot(data = diamonds, aes(x = cut, color = clarity)) +
  geom_bar(position = "identity", fill = NA)
#2
ggplot(data = diamonds) +
  geom_bar(aes(x = cut, fill = clarity), position = "fill")
#3
ggplot(data = diamonds) +
  geom_bar(aes(x = cut, fill = clarity), position = "dodge")

#Way to avoid over-plotting (the two graphs are slightly different):
ggplot(data = mpg, aes(x = displ, y = hwy)) +
  geom_point(position = "jitter")

ggplot(data = mpg, aes(x = displ, y = hwy)) +
  geom_jitter()

#A (horizontal) boxplot, with the coordinates flipped:
ggplot(data = mpg, aes(x = class, y = hwy)) +
  geom_boxplot() + coord_flip()

#Using coord_quickmap() to set the correct aspect ration for a map
world <- map_data("world")
ggplot(world, aes(long, lat, group = group)) +
  geom_polygon(fill = "white", color = "black") +
  coord_quickmap()

#An example on coord_polar():
bar <- ggplot(data = diamonds) +
  geom_bar(aes(x = cut, fill = cut), show.legend = FALSE,
           width = 1) +
  theme(aspect.ratio = 1) +
  labs(x = NULL, y = NULL)
bar
bar + coord_flip()
bar + coord_polar()

#Turning a stacked bar chart into a pie chart:
ggplot(data = diamonds) +
  geom_bar(aes(x = cut, fill = clarity), position = "stack") +
  coord_polar()
